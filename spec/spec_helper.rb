require "simplecov"

$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)
require "exchange_rate"

ExchangeRate.configure do |config|
  config.provider = :european_central_bank
  config.verbose = false
end

RSpec.configure do |config|
  config.filter_run :focus
  config.run_all_when_everything_filtered = true
  config.order = :random

  Kernel.srand config.seed

  config.expect_with :rspec do |expectations|
    expectations.syntax = :expect
  end

  config.mock_with :rspec do |mocks|
    mocks.syntax = :expect
    mocks.verify_partial_doubles = true
  end
end

