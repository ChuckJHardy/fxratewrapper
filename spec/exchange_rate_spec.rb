require "vcr_helper"

describe ExchangeRate do
  let(:instance) { described_class.new }

  describe ".at" do
    subject(:endpoint) { described_class.at("2015-05-14", "USD", "GBP") }

    it "returns rate" do
      VCR.use_cassette("valid/search") do
        expect(endpoint.fetch("rate")).to eq("1.579173005116857973")
      end
    end
  end

  describe "#dates" do
    subject(:endpoint) { instance.dates }

    it "returns dates" do
      VCR.use_cassette("valid/dates") do
        expect(endpoint.fetch("dates").first).to eq("2015-05-14")
        expect(endpoint.fetch("dates").count).to eq(61)
      end
    end
  end

  describe "#currencies" do
    subject(:endpoint) { instance.currencies }

    it "returns currencies" do
      VCR.use_cassette("valid/currencies") do
        expect(endpoint.fetch("currencies").first).to eq("USD")
        expect(endpoint.fetch("currencies").count).to eq(31)
      end
    end
  end
end
