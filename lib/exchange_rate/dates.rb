class ExchangeRate
  class Dates < DTO
    def endpoint
      "providers/#{provider}/dates"
    end

    private

    def provider
      ExchangeRate.configuration.provider
    end
  end
end
