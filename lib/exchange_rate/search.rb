class ExchangeRate
  class Search < DTO
    def endpoint
      "search?provider=#{provider}&date=#{date}&base=#{base}&counter=#{counter}"
    end

    private

    def date
      options.fetch(:date)
    end

    def base
      options.fetch(:base)
    end

    def counter
      options.fetch(:counter)
    end

    def provider
      ExchangeRate.configuration.provider
    end
  end
end
