require "faraday"
require "faraday_middleware"

class ExchangeRate
  class API
    def self.get(*args)
      new.get(*args)
    end

    def get(url:, options: {})
      connection.get(URI.escape(url), options)
    end

    private

    def domain
      ExchangeRate.configuration.domain
    end

    def verbose?
      ExchangeRate.configuration.verbose
    end

    def connection
      Faraday.new(url: domain) do |faraday|
        faraday.use Faraday::Response::Logger if verbose?

        faraday.response :json, content_type: /\bjson$/

        faraday.adapter Faraday.default_adapter
      end
    end
  end
end
