class ExchangeRate
  class Currencies < DTO
    def endpoint
      "providers/#{provider}/currencies"
    end

    private

    def provider
      ExchangeRate.configuration.provider
    end
  end
end
