class ExchangeRate
  module Configure
    def configuration
      @configuration ||= Configuration.new
    end

    def configure
      yield(configuration)
      configuration
    end

    private

    class Configuration
      attr_accessor :domain, :provider, :verbose

      def initialize
        self.domain = "http://localhost:9299"
        self.provider = nil
        self.verbose = false
      end
    end
  end
end
