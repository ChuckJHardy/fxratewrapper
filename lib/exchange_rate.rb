require "exchange_rate/configuration"
require "exchange_rate/dto"
require "exchange_rate/search"
require "exchange_rate/dates"
require "exchange_rate/currencies"

class ExchangeRate
  extend Configure

  def self.at(date = Date.today, base, counter)
    new.search(date: date, base: base, counter: counter)
  end

  def search(*args)
    Search.get(*args)
  end

  def dates(*args)
    Dates.get(*args)
  end

  def currencies(*args)
    Currencies.get(*args)
  end
end
