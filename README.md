# FXRateWrapper

Wrapper for the FXRate Service.

## Installation

Add this line to your application's Gemfile:

    gem 'exchange_rate', git: 'git@bitbucket.org:ChuckJHardy/fxratewrapper.git'

And then execute:

    $ bundle

## Configuration

    ExchangeRate.configure do |config|
      config.domain = "http://localhost:1234"
      config.provider = :european_central_bank
    end

* `domain` sets the base uri for fx rate. `https://localhost:9299`
* `provider` sets the exchange rate provider `nil`
* `verbose` sets the logger level `false`

## Usage

Find Exchange Rate:

    ExchangeRate.at("2015-05-14", "GBP", "USD")
    # => { "rate" => "0.81716" }

Get List of Dates:

    wrapper = ExchangeRate.new
    wrapper.dates
    # => { "dates" => [] }

Get List of Currencies:

    wrapper = ExchangeRate.new
    wrapper.currencies
    # => { "currencies" => [] }

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release` to create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

1. Fork it ( https://github.com/[my-github-username]/fxratewrapper/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
